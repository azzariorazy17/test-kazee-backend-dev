## API Reference

#### Get all cars

```http
  GET /cars
```

#### Get car by id

```http
  GET /car/${id}
```

#### Login

```http
  POST /login
```

#### Logout

```http
  POST /logout
```
