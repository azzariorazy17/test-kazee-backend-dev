import { error_response, success_response } from "../helpers/response-object.js";
import cars from "../model/Car.js";

export const getCars = async (req, res) => {
    try {
        const data = await cars.findAll();

        return res.status(200).send(success_response(data, 200, "Fetch data success"));
    } catch (error) {
        return res.status(500).send(error_response(500, "Server error"));
    }
}

export const getCarById = async (req, res) => {
    try {
        const data = await cars.findOne({
            where: {
                id: req.params.id
            }
        });

        if(data) {
            return res.status(200).send(success_response(data, 200, "Fetch data success"));
        } else {
            return res.status(404).send(error_response(404, "Data not found"));
        }
    } catch (error) {
        return res.status(500).send(error_response(500, "Server error"));
    }
}