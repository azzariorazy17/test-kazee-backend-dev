import express from "express";
import db from "./config/database.js";
import cars from "./model/Car.js";
import users from "./model/User.js";
import router from "./routes/index.js";
import { QueryTypes } from "sequelize";
import bcrypt from "bcrypt";
import moment from "moment";
import cookieParser from "cookie-parser";

const app = express();
const port = process.env.PORT;

app.use(express.json());
app.use(cookieParser());
app.use(router);

try {
    await db.authenticate();
    console.info("Database is working");

    await db.sync();

    // add default user
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash("johndoe", salt);
    const now = moment().format('YYYY-MM-DD HH:mm:ss');

    await db.query(`
        INSERT INTO users(name, email, password, createdAt, updatedAt) VALUES('John Doe', 'johndoe@mail.com', '${password}', '${now}', '${now}');
    `, {
        type: QueryTypes.SELECT
    });

    // add default cars
    await db.query(`
        INSERT INTO cars(name, tier, brand, transmission, type, createdAt, updatedAt) 
        VALUES
        ('Civic Type R', 'R', 'Honda', 'automatic', 'sport', '${now}', '${now}'),
        ('Kia Rio', '', 'Kia', 'automatic', 'hatchback', '${now}', '${now}');
    `, {
        type: QueryTypes.SELECT
    });
} catch(error) {
    console.error(error);
}

app.listen(port, () => {
    console.info(`Server is running on port ${port}`);
});