import jwt from "jsonwebtoken";
import { error_response } from "../helpers/response-object.js";

export const verifyAuth = (req, res, next) => {
    const token = req.cookies.token;

    if(!token) {
        return res.status(401).send(error_response(401, "Not authenticated"));
    }

    jwt.verify(token, process.env.SECRET_TOKEN, (err, payload) => {
        if(err) {
            return res.status(403).send(error_response(403, "Invalid token"));
        }

        req.user = {
            id: payload.user_id
        };

        next();
    });
} 