import express from "express";
import { login, logout } from "../controller/AuthController.js";
import { getCarById, getCars } from "../controller/CarsController.js";
import { verifyAuth } from "../middleware/verifyAuth.js";

const router = express.Router();

// cars
router.get('/cars', verifyAuth, getCars);
router.get('/car/:id', verifyAuth, getCarById);

// auth
router.post('/login', login);
router.post('/logout', verifyAuth, logout);

export default router;