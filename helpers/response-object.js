export const success_response = (data, status_code, message) => {
    return {
        "success": true,
        "message": message,
        "data": data,
        "status_code": status_code
    }
};

export const error_response = (status_code, message) => {
    return {
        "success": false,
        "message": message,
        "status_code": status_code
    }
};