import { Sequelize } from "sequelize";
import db from "../config/database.js";

const { DataTypes } = Sequelize;

const cars = db.define("cars", {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    tier: {
        type: DataTypes.STRING,
        allowNull: true
    },
    brand: {
        type: DataTypes.STRING,
        allowNull: false
    },
    transmission: {
        type: DataTypes.ENUM,
        values: ['manual', 'automatic'],
        allowNull: false
    },
    type: {
        type: DataTypes.ENUM,
        values: ['mpv', 'sedan', 'hatchback', 'sport', 'crossover', 'suv'],
        allowNull: false
    },
});

export default cars;