import users from "../model/User.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { error_response, success_response } from "../helpers/response-object.js";

export const login = async (req, res) => {
    try {
        const req_email = req.body.email;
        const password = req.body.password;

        if(!req_email || !password) {
            return res.status(400).send(error_response(400, "Email or Password is incorrect"));
        }

        const data = await users.findOne({
            where: {
                email: req_email
            }
        });

        if(!data) {
            return res.status(404).send(error_response(404, "Email not found"));
        }

        const check_password = await bcrypt.compare(password, data.password);

        if(!check_password) {
            return res.status(400).send(error_response(400, "Email or Password is incorrect"));
        }

        const user_id = data.id;
        const name = data.name;
        const email = data.email;

        const token = jwt.sign({
            user_id
        }, process.env.SECRET_TOKEN, {
            expiresIn: "1d"
        });

        res.cookie("token", token, {
            httpOnly: true
        }).status(200).send(success_response({
            name,
            email
        }, 200, "Login successfully"));

    } catch (error) {
        return res.status(500).send(error_response(500, "Server error"));
    }
}

export const logout = (req, res) => {
    res.clearCookie('token');

    return res.status(200).send(success_response(null, 200, "Logout successfully"));
}